package com.qf.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Hang
 * @description 商品实体
 * @date 2023/5/12 17:37
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    private Integer pId;
    private String pName;
    private Double pPrice;
    private String pDesc;

}
