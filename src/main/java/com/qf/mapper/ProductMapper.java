package com.qf.mapper;

import com.qf.pojo.Product;

import java.util.List;

/**
 * @author Hang
 * @description 商品数据访问接口
 * @date 2023/5/12 17:47
 */

public interface ProductMapper {
    List<Product>selectAllProducts();
}
