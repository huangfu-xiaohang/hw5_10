package com.qf.service;

import com.qf.pojo.Product;

import java.util.List;

/**
 * @author Hang
 * @description 商品的业务接口
 * @date 2023/5/12 17:50
 */

public interface ProductService {
    List<Product>getAllProducts();
}
