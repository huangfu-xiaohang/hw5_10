package com.qf.controller;

import com.alibaba.fastjson.JSONObject;
import com.qf.pojo.Product;
import com.qf.service.ProductService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;

@WebServlet("/findAll")
public class FindAllServlet extends HttpServlet {
    private ProductService productService ;
    //当前在访问"/findAll"通过web容器创建Servlet对象
    //执行无参构造方法
    public FindAllServlet(){
        //读取spring-config.xml核心配置文件
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-config.xml");

        productService = applicationContext.getBean(ProductService.class);

    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



        //调用业务接口
        List<Product> allProducts = productService.getAllProducts();

        //Fastjson
        JSONObject object = new JSONObject() ;
        String str = object.toJSONString(allProducts);

        //设置响应乱码
        response.setContentType("application/json;charset=utf-8");
        response.getWriter().write(str);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }
}
