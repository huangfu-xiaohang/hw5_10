package com.qf.service.impl;

import com.qf.mapper.ProductMapper;
import com.qf.pojo.Product;
import com.qf.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author Hang
 * @description 商品业务实现接口
 * @date 2023/5/12 19:30
 */

public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductMapper productMapper ;
    @Override
    public List<Product> getAllProducts() {
        List<Product> products = productMapper.selectAllProducts();
        if(products!=null || products.size()>0){
            return products ;
        }
        return null ;

    }

}
